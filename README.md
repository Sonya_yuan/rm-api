//1. env database set up

//2. plugins/lib/db/knexfileCli.js database set up

//3. migrate:
	knex migrate:latest --knexfile ./plugins/lib/db/knexfileCli.js

//4. migrate-rollback:
	knex migrate:rollback --knexfile ./plugins/lib/db/knexfileCli.js

//5. seed:
	knex seed:run --knexfile ./plugins/lib/db/knexfileCli.js