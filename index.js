'use strict';

const Blipp = require("blipp");
const good = require("good");
const inert = require("@hapi/inert");
const Vision = require("@hapi/vision");
const dotenv = require("dotenv");
const Hapi = require("@hapi/hapi");
const appPlugins = require("./plugins");
const HapiSwagger = require("hapi-swagger");

dotenv.config();

const init = async () => {

  const server = Hapi.server({
    port: process.env.PORT,
    host: 'localhost'
  });

  const swaggerOptions = {
    info: {
      title: "RM Data API Documentation",
      version: process.env.APP_VERSION,
    },
    host: process.env.SWAGGER_HOSTNAME,
    schemes: [process.env.SWAGGER_SCHEMES],
    definitionPrefix: "useLabel",
    grouping: "tags",
    tags: [{
      "name": "auth",
      "description": "Authentication for APIs"
    }, {
      "name": "users",
      "description": "Users CRUD"
    }, {
      "name": "resume",
      "description": "Resume CRUD"
    }, {
      "name": "resumes",
      "description": "Resumes CRUD"
    }],
    // sortTags: "name",
  };

  let plugins = [
    {
      plugin: inert,
      options: {},
    },
    {
      plugin: Vision,
      options: {},
    },
    {
      plugin: HapiSwagger,
      options: swaggerOptions,
    },
    {
      plugin: appPlugins,
      routes: {
        prefix: "/v1",
      },
    },
    // {
    //   plugin: healthPlugin,
    // },
  ];

  if ("testing" !== process.env.NODE_ENV) {
    const nonTestingPlugins = [
      {
        plugin: Blipp,
        options: {
          showAuth: true,
          showStart: true,
        },
      },
      {
        plugin: good,
        options: {
          reporters: {
            console: [
              {
                module: "good-squeeze",
                name: "Squeeze",
                args: [{
                  response: "*",
                  log: "*",
                }],
              },
              {
                module: "good-console",
              },
              "stdout",
            ],
          },
        },
      },
    ];
    plugins = plugins.concat(nonTestingPlugins);
  }
  
  // include our module here ↓↓, for example, require('hapi-auth-jwt2')
  await server.register(require('hapi-auth-jwt2'));
  server.auth.strategy('jwt', 'jwt', 
    {
      key: process.env.JWT_PRIVATE_KEY, 
      validate: (decoded, request) => {
        console.log(" - - - - - - - DECODED token:");
        console.log(decoded);
        console.log(" - - - - - - - request route:");
        console.log(request.route.settings.auth);

        return { isValid: true };
      },
      verifyOptions: { algorithms: [ 'HS256' ] }, // only allow HS256 algorithm
    }
  );

  server.auth.default('jwt');    
  await server.register(plugins);
  await server.start();
  console.log('Server running at:', server.info.uri);
};

process.on('unhandledRejection', (err) => {

  console.log(err);
  process.exit(1);
});

init();