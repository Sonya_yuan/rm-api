"use strict";

const boom = require("boom");
const bufferConcat = require("buffer-concat");
const knex = require("knex");
const objection = require("objection");
const db = require("./lib/db/index.js");
const handler = require("./handlers");
const docx = require('docx');
const fs = require('fs');
const generateWord = require("../plugins/handlers/lib/generateWord");
const generatePdf = require("../plugins/handlers/lib/generatePdf");
const simpleTemplateFile = require("../plugins/handlers/lib/wordTemplateHelper");
const libre = require("libreoffice-convert");
const path = require("path");
const pdfMake = require("pdfMake");
const Joi = require('@hapi/joi');


const routeConfig = {
  cors: {
      origin: ['http://localhost:3000'],
      additionalHeaders: ['cache-control', 'x-requested-with']
  }
};

const corsConfig = {
  origin: ['http://localhost:3000'],
  additionalHeaders: ['cache-control', 'x-requested-with'],  
};

const plugin = {
  name: "um-data-api-v1",
  version: "1.0.0",
  register: (server, options) => {
    server.app.Boom = boom;
    server.app.db = db;
    server.app.knex = knex;
    server.app.objection = objection;
    server.app.docx = docx;
    server.app.fs = fs;
    server.app.generateWord = generateWord;
    server.app.generatePdf = generatePdf;
    server.app.simpleTemplateFile = simpleTemplateFile;
    server.app.libre = libre;
    server.app.path = path;
    server.app.pdfMake = pdfMake;
    server.app.bufferConcat = bufferConcat;

    server.route([
      {
        method: 'POST',
        path: '/token',
        handler: handler.getToken,
        config: {
          description: 'Get Json Web Token',
          notes: 'get JWT token',
          tags: [
              'api', 'auth'
          ],
          validate: {
            payload: Joi.object({
              clientId: Joi.string().required().description('Client App Id'),
              clientSecret: Joi.string().description('Client App Secret'),
              userName: Joi.string().required().description('User Name'),
              userPass: Joi.string().required().description('Password'),
            })
          },
          cors: corsConfig,
          auth: false,
        }
      },
      {
        method: 'GET',
        path: '/user/{id?}',
        handler: handler.getUser,
        config: {
          description: 'Get a specific user by id',
          notes: 'input a user id to get a specific user details',
          tags: [
              'api', 'users'
          ],
          validate: {
            params: Joi.object({
              id: Joi.number().description('the id for the user id'),
            })
          },
          cors: corsConfig,
        } 
      },
      {
        method: 'GET',
        path: '/users',
        handler: handler.getUsers,
        config: {
          description: 'Get user list',
          notes: 'get user list',
          tags: [
            'api', 'users'
          ],
          validate: {
            // headers: Joi.object({
            //   'authorization': Joi.string().required().description('JWT Token')
            // }),
          },
          cors: corsConfig,
          auth: {
            strategies: ['jwt'],
            scope: ['system-admin', 'user']
          }
        } 
      },
      {
        method: 'POST',
        path: '/user',
        handler: handler.postUsers,
        config: {
          description: 'add a specific user',
          notes: 'add a specific user',
          tags: [
            'api', 'users'
          ],
          validate: {
            payload: Joi.object({
                name: Joi.string().min(1).max(20).required().description('name'),
               userName: Joi.string().min(1).max(20).required().description('user name'),
                password: Joi.string().min(6).max(50).description('Password'),
                role: Joi.string().description('user role'),
            }),
          },
          cors: corsConfig
        } 
      },
      {
        method: 'POST',
        path: '/user/{id}',
        handler: handler.putUser,
        config: {
          description: 'update a specific user',
          notes: 'update a specific user',
          tags: [
            'api', 'users'
          ],
          validate: {
            params: Joi.object({
              id: Joi.number().description('the id for the user id'),
            }),
            payload: Joi.object({
                userName: Joi.string().min(1).max(20).required().description('user name'),
                password: Joi.string().max(50).description('Password'),
                role: Joi.string().description('user role'),
            }),
          },
          cors: corsConfig,
          // auth: {
          //   strategies: ['jwt'],
          //   scope: ['manager']
          // }
        } 
      },
      {
        method: 'Post',
        path: '/resume',
        handler: handler.postResume,
        options: {
          description: 'Insert or Update a specific resume',          
          tags: [
            'api', 'insert or update resume'
          ],
          cors: corsConfig
        } 
      },
      {
        method: 'GET',
        path: '/resume/{id}',
        handler: handler.getResume,
        config: {
          description: 'Get a specific resume',          
          tags: [
            'api', 'resume'
          ],
          cors: corsConfig
        } 
      },
      {
        method: 'Post',
        path: "/resumes/generate",
        handler: handler.generateUserOwnResume,
        config: {
          description: 'Generate a user own resume',
          notes: 'generate a user own resume',
          tags: [
            'api', 'resumes'
          ],
          validate: {
            payload: Joi.object({
                format: Joi.string().min(1).max(20).description('Resume file format: PDF'),
                resumeId: Joi.number().required().description('Resume ID'),
            }),
          },
          cors: corsConfig,
        } 
      },
      {
        method: 'Post',
        path: "/resumes/generateSpecificResume",
        handler: handler.generateUserResume,
        config: {
          description: 'Generate a specific user\'s resume',
          notes: 'generate a specific user\'s resume',
          tags: [
            'api', 'resumes'
          ],
          validate: {
            payload: Joi.object({
                format: Joi.string().min(1).max(20).required().description('Resume file format: PDF'),
                resumeId: Joi.number().required().description('Resume ID'),
            }),
          },
          cors: corsConfig,
          auth: {
            strategies: ['jwt'],
            scope: ['system-admin']
          }
        } 
      },
      // {
      //   method: 'GET',
      //   path: '/resumes/getResumeFile/{resumeId}/{format}',
      //   handler: handler.getResumeFile,
      //   config: {
      //     description: 'Get a specific resume file',
      //     notes: 'Get a specific resume file',
      //     tags: [
      //         'api', 'Get resume file'
      //     ],
      //     cors: corsConfig,
      //   }
      // },
      {
        method: 'GET',
        path: '/resumes/getBase64ResumeFile/{resumeId}/{format}',
        handler: handler.getBase64ResumeFile,
        config: {
          description: 'Get a specific resume file return base64',
          notes: 'Get a specific resume file',
          tags: [
              'api', 'Get resume file'
          ],
          cors: corsConfig,
        }        
      },
      {
        method: 'GET',
        path: "/resumes",
        handler: handler.getResumes,
        config: {
          description: 'Get resume list',          
          tags: [
            'api', 'resumes'
          ],
          cors: corsConfig
        } 
      },
      // {
      //   method: 'GET',
      //   path: '/getResumeAssessment/{id}/{format}',
      //   handler: handler.getResumeAssessment,
      //   config: {
      //     description: 'test',
      //     notes: 'testttt',
      //     tags: [
      //         'api', 'testttt'
      //     ],
      //     cors: corsConfig,
      //     auth: false,
      //   }
      // },
      // {
      //   method: 'GET',
      //   path: '/getResumeAssessment/latest/{format}',
      //   handler: handler.getResumeAssessment,
      //   options: routeConfig
      // },
      
    ]);
  },
};

module.exports = {
  plugin,
};
