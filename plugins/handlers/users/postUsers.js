'use strict';
const fs = require('fs');
const md5 = require('md5-node');

async function postUsers (request, h) {
  const { User } = request.server.app.db.model;
  const { payload, params } = request;
  
  console.log(params);
  console.log(payload);
  console.log(payload.password);
  const id = payload.id;
  const password = payload.password
  ? md5(payload.password, process.env.PASSWORD_MD5_KEY)
  : 'e10adc3949ba59abbe56e057f20f883e';

  const newUser = {
    name: payload.name,
    user_name: payload.userName,
    password,
    role: payload.role
  }

  let data;
  let errorMessage;
  try{
    if (!id) {
      console.log('==================== create user');
      data = await User.query().insert(newUser);
      if (data.id) {
        // directory path
        const dir = process.env.USER_RESUME_FOLDER + '/' + data.id;
        // create new directory
        try {
            // first check if directory already exists
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            } else {
                console.log("Directory already exists.");
            }
        } catch (err) {
          errorMessage='Generate resume file failed'
            console.log(err);
        }
      }
    }
  }
  catch(err) {
    if(err.constraint==='users_name_unique'){
      errorMessage='user name duplicated'
    }
    console.log(err)
  }

  return h.response(data==undefined?{"errorMessage":errorMessage}:{"userName": data.name, "role": data.role});
}

module.exports = postUsers;