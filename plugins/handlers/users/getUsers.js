'use strict';

async function getUsers (request, h) {
  const { User } = request.server.app.db.model;
  const { query } = request;

  const limit = query.limit? query.limit: 10;
  const page = query.page? query.page: 0;
  const data = await User.query()
  .orderBy("id", "desc")
  .limit(limit)
  .offset(page)
  .catch((ex) => {
    console.log(ex);
  })


  return h.response(data);

}

module.exports = getUsers;
