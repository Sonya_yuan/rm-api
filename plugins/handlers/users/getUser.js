'use strict';

async function getUser (request, h) {
  const { User } = request.server.app.db.model;
  const { params } = request;
  const { Boom } = request.server.app;

  const userId = params.id ? params.id : 0;

  if(!userId) {
    throw Boom.notFound("User not found");
  }

  let data;
  try{
    data = await User.query()
    .where('id',userId);
  }
  catch(err) {
    console.log(err)
  }

  return h.response(data[0]);

}

module.exports = getUser;