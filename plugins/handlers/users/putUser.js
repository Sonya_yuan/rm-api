'use strict';
const md5 = require('md5-node');

async function putUser (request, h) {
  const { User } = request.server.app.db.model;
  const { payload, params } = request;  
  const id = params.id;

  const updateUser = {
    user_name: payload.userName,
    role: payload.role
  }
  if (payload.password) {
    updateUser.password = md5(payload.password, process.env.PASSWORD_MD5_KEY);
  }

  let data;
  try {
    if (!id) {
      console.log('==================== no user');
    }
    else {
      console.log('==================== update a user:', id);
      updateUser.id = id;
      data = await User
      .query()
      .findById(id)
      .patch(updateUser);
    }
  }
  catch (err) {
    console.log(err)
  }

  return h.response(data);

}

module.exports = putUser;