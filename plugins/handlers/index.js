"user strict";

const getUsers = require("./users/getUsers.js");
const postUsers = require("./users/postUsers.js");
const putUser = require("./users/putUser.js");
const postResume = require("./resume/postResume");
const getResume = require("./resume/getResume");
const getResumes = require("./resumes/getResumes.js");
const getResumeAssessment = require("./resumes/getResumeAssessment");
const getResumeAssessmentPdf = require("./resumes/getResumeAssessmentPdf");
const getUser = require("./users/getUser");
const { getToken } = require("./auth/getToken");
const generateUserOwnResume = require("./resumes/generateUserOwnResume");
const generateUserResume = require("./resumes/generateUserResume");
const getResumeFile = require("./resumes/getResumeFile");
const getBase64ResumeFile = require("./resumes/getBase64ResumeFile")

module.exports = {
  getUser,
  getUsers,
  postUsers,
  putUser,
  getResume,
  postResume,
  getResumes,
  getResumeAssessment,
  getResumeAssessmentPdf,
  getToken,
  generateUserOwnResume,
  generateUserResume,
  getResumeFile,
  getBase64ResumeFile,
};
