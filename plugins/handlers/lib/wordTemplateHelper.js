"use strict";
const moment = require("moment");
const PizZip = require("pizzip");
const Docxtemplater = require("docxtemplater");

const fs = require("fs");
const path = require("path");
// The error object contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
const replaceErrors = function (key, value) {
  if (value instanceof Error) {
    return Object.getOwnPropertyNames(value).reduce(function (error, key) {
      error[key] = value[key];
      return error;
    }, {});
  }
  return value;
};

const errorHandler = function (error) {
  console.log(JSON.stringify({ error: error }, replaceErrors));

  if (error.properties && error.properties.errors instanceof Array) {
    const errorMessages = error.properties.errors
      .map(function (error) {
        return error.properties.explanation;
      })
      .join("\n");
    console.log("errorMessages", errorMessages);
    // errorMessages is a humanly readable message looking like this :
    // 'The tag beginning with "foobar" is unopened'
  }
  throw error;
};

const getGeneratedResumeFileName = function (resumeId) {
  return Date.now() + "_" + resumeId + ".docx";
};

const getGeneratedResumeFilePath = function (userId, fileName) {
  //
  const filePath =
    process.env.USER_RESUME_FOLDER + "/" + userId + "/" + fileName;

  return filePath;
};

const getResumeTemplateFile = function (templateName) {
  //
  const templateFile = path.resolve(
    __dirname,
    "../../../templates/" + templateName
  );

  return templateFile;
};

const getUserSelfEvaluation = function (resume) {
  let array = [];
  if (
    resume.metadata.self_evaluations &&
    Array.isArray(resume.metadata.self_evaluations)
  ) {
    resume.metadata.self_evaluations.map((value, index) => {
      array.push({ feature_item: value.line });
    });
  }

  return array;
};

const getUserTechnicalSummary = function (resume) {
  let array = [];
  if (
    resume.metadata.technical_summary &&
    Array.isArray(resume.metadata.technical_summary)
  ) {
    resume.metadata.technical_summary.map((technical, index) => {
      let technicalObj = { name: technical.name, tags: [] };

      technical.tags.map((tag) => {
        technicalObj.tags.push({ tag_item: tag });
      });

      array.push(technicalObj);
    });
  }

  return array;
};

const getUserResponsiveRoles = function (resume) {
  let array = [];
  if (
    resume.metadata.responsive_roles &&
    Array.isArray(resume.metadata.responsive_roles)
  ) {
    resume.metadata.responsive_roles.map((value, index) => {
      array.push({ role_item: value });
    });
  }

  return array;
};

const getUserProjectExperiences = function (resume) {
  let array = [];
  if (resume.metadata.project_experiences) {
    resume.metadata.project_experiences.map((item, index) => {
      array.push({
        project_period: item.start_time + " - " + item.end_time || "now",
        project_name: item.name,
        project_description: item.description,
        project_experience_summary: getProjectSkillSets(item),
      });
    });
  }

  return array;
};

const getProjectSkillSets = function (project) {
  let array = [];
  if (project.lines) {
    project.lines.map((item, index) => {
      array.push({
        name: item.name,
        tags: item.tags.map((tag) => {
          return { tag_item: tag };
        }),
      });
    });
  }

  return array;
};

const getUserEducations = function (resume) {
  let array = [];
  if (
    resume.metadata.education_experiences &&
    Array.isArray(resume.metadata.education_experiences)
  ) {
    resume.metadata.education_experiences.map((item, index) => {
      array.push({
        start_time: moment(item.start_time).format("MM.YYYY"),
        end_time: moment(item.end_time).format("MM.YYYY"),
        school_name: item.school_name,
        major: item.major,
      });
    });
  }

  return array;
};

const getEducationLanuageSets = function (resume) {
  let array = [];
  if (resume.metadata.technical_summary) {
    if (resume.metadata.languages && Array.isArray(resume.metadata.languages)) {
      resume.metadata.languages.map((language, index) => {
        array.push({
          language_name: language.name,
          language_description: language.description,
        });
      });
    }
  }

  return array;
};

const writeResumeIntoTemplate = function (resume, templateFile, userId = 1) {
  // Load the docx file as a binary
  const content = fs.readFileSync(templateFile, "binary");
  //
  const doc = new Docxtemplater();
  try {
    let zip = new PizZip(content);
    doc.loadZip(zip);
    // set the template variables
    doc.setData({
      experience_level: "Senior Software Engineer",
      full_name: resume.metadata.name || "John",
      feature_list: getUserSelfEvaluation(resume),
      technical_summary: getUserTechnicalSummary(resume),
      // technical_summary: [
      //     { name: 'languageAAA', tags: [{ tag_item: 'C#' }, { tag_item: 'JS' }, { tag_item: 'HTML & CSS3' }] },
      //     { name: 'DBs', tags: [{ tag_item: 'C#1' }, { tag_item: 'JS2' }, { tag_item: 'HTML & CSS3333' }] }
      // ],
      responsive_roles: getUserResponsiveRoles(resume),
      project_experiences: getUserProjectExperiences(resume),
      educations: getUserEducations(resume),
      education_languages: getEducationLanuageSets(resume),
      // education_languages:[
      //     { language_name: 'languageAAA', language_description:'description1' },
      //     { language_name: 'languageBBB', language_description:'description2' },
      // ],
      
    });
    // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
    doc.render();
  } catch (error) {
    // Catch rendering errors (errors relating to the rendering of the template : angularParser throws an error)
    errorHandler(error);
  }
  //
  let buf = doc.getZip().generate({ type: "nodebuffer" });
  // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
  return buf;

  //do not need save file, just return buff
  // const outputFileName = getGeneratedResumeFileName(resume.id);
  // fs.writeFileSync(getGeneratedResumeFilePath(userId, outputFileName), buf);

  // return outputFileName;
};

const applyDefaultResumeTemplate = function (resume, userId) {
  //
  const templateFile = getResumeTemplateFile("resume.docx");

  return writeResumeIntoTemplate(resume, templateFile, userId);
};

const applySpecificResumeTemplate = function (resume, templateName, userId) {
  //
  const templateFile = getResumeTemplateFile(templateName);

  return writeResumeIntoTemplate(resume, templateFile, userId);
};

const generateWordDoc = function (resume, userId, useDefaultTemplate = true) {
  //
  if (useDefaultTemplate) {
    return applyDefaultResumeTemplate(resume, userId);
  } else {
    return applySpecificResumeTemplate(resume, "", userId);
  }
};

module.exports = generateWordDoc;
