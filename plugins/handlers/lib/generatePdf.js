"use strict";

class Adapter {
  constructor(component, resume) {
    this.pdfMake = component.pdfMake
    this.fs = component.fs
    this.resume = resume.metadata
    this.bufferConcat = component.bufferConcat
  }

  generatePersonalInfo() {
    let personalInfo = []

    const lineOne = {
      columns: [
        {
          width: '*',
          text: 'Name'
        },
        {
          width: '*',
          text: this.resume.name
        },
        {
          width: '*',
          text: 'Birthday'
        },
        {
          width: '*',
          text: this.resume.birthday
        }
      ]
    }

    personalInfo.push(lineOne)

    const lineTwo = {
      columns: [
        {
          width: '*',
          text: 'Gender'
        },
        {
          width: '*',
          text: this.resume.gender
        },
        {
          width: '*',
          text: 'Phone'
        },
        {
          width: '*',
          text: this.resume.phone
        }
      ]
    }

    personalInfo.push(lineTwo)

    const lineThree = {
      columns: [
        {
          width: '*',
          text: 'Email'
        },
        {
          width: '*',
          text: this.resume.email
        }
      ]
    }

    personalInfo.push(lineThree)

    return personalInfo
  }

  generateEducationExperience() {

    let educationExperiences = []
    if(
      !this.resume
      || !this.resume.education_experience
      || !this.resume.education_experience.length
    ) {
      return educationExperiences
    }

    this.resume.education_experience.forEach((item) => {
      const lineOne = {
        columns: [
          {
            width: '*',
            text: 'School Name'
          },
          {
            width: '*',
            text: item.school_name
          }
        ]
      }
      educationExperiences.push(lineOne)

      const lineTwo = {
        columns: [
          {
            width: '*',
            text: 'Major'
          },
          {
            width: '*',
            text: item.major
          }
        ]
      }
      educationExperiences.push(lineTwo)

      const lineThree = {
        columns: [
          {
            width: '*',
            text: 'Start Time'
          },
          {
            width: '*',
            text: item.start_time
          }
        ]
      }
      educationExperiences.push(lineThree)

      const lineFour = {
        columns: [
          {
            width: '*',
            text: 'End Time'
          },
          {
            width: '*',
            text: item.end_time
          }
        ]
      }
      educationExperiences.push(lineFour)
    })
    return educationExperiences
  }

  generateWorkExperience() {

    let workExperiences = []

    if(
      !this.resume
      || !this.resume.work_experience
      || !this.resume.work_experience.length
    ) {
      return workExperiences
    }

    this.resume.work_experience.forEach((item) => {
      const lineOne = {
        columns: [
          {
            width: '*',
            text: 'Company Name'
          },
          {
            width: '*',
            text: item.company_name
          }
        ]
      }
      workExperiences.push(lineOne)

      const lineTwo = {
        columns: [
          {
            width: '*',
            text: 'Position'
          },
          {
            width: '*',
            text: item.position
          }
        ]
      }
      workExperiences.push(lineTwo)

      const lineThree = {
        columns: [
          {
            width: '*',
            text: 'Position'
          },
          {
            width: '*',
            text: item.position
          }
        ]
      }
      workExperiences.push(lineThree)

      const lineFour = {
        columns: [
          {
            width: '*',
            text: 'Skill'
          },
          {
            width: '*',
            text: item.skill_set
          }
        ]
      }
      workExperiences.push(lineFour)

      const lineFive = {
        columns: [
          {
            width: '*',
            text: 'Skill'
          },
          {
            width: '*',
            text: item.skill_set
          }
        ]
      }
      workExperiences.push(lineFive)

      const lineSix = {
        columns: [
          {
            width: '*',
            text: 'Start Time'
          },
          {
            width: '*',
            text: item.start_time
          }
        ]
      }
      workExperiences.push(lineSix)

      const lineSeven = {
        columns: [
          {
            width: '*',
            text: 'End Time'
          },
          {
            width: '*',
            text: item.end_time
          }
        ]
      }
      workExperiences.push(lineSeven)
    });

    return workExperiences
  }
  async generatePdf() {

    const personalInfo = this.generatePersonalInfo();
    const educationExperiences = this.generateEducationExperience();
    const workExperiences = this.generateWorkExperience();

    const docDefinitionContent = personalInfo.concat(
      educationExperiences,
      workExperiences
      )
    const docDefinition = {
      content: docDefinitionContent
    }

    // Used to export the file into a .docx file
    const b64string = await this.getBase64PdfString(docDefinition)

    return b64string
  }

  getBase64PdfString(docDefinition) {
    let size = 0;
    const bufferChunks = []


    const fonts =  {
      Roboto: {
        normal: 'fonts/Roboto-Regular.ttf',
        bold: 'fonts/Roboto-Medium.ttf',
        italics: 'fonts/Roboto-Italic.ttf',
        bolditalics: 'fonts/Roboto-MediumItalic.ttf'
      }
    }

    const pdfPrinter = new this.pdfMake(fonts)
    let pdfDoc
    try{
      pdfDoc = pdfPrinter.createPdfKitDocument(docDefinition)
    }
    catch(err) {
      console.log(err)
      throw err
    }

    const bufferConcat = this.bufferConcat;

    pdfDoc.on('readable', function () {
      const data = pdfDoc.read()
      if (data) {
        size += data.length
        bufferChunks.push(data)
      }
    })
    pdfDoc.end();

    return new Promise((resolve, reject) => {
      pdfDoc.on('end', function () {
        const pdfBuffer = bufferConcat(bufferChunks, size);
        const base64PdfString = pdfBuffer.toString('base64')
        resolve(base64PdfString)
      })
    })
  }
}

module.exports = {
  Adapter
}