"use strict";

const { WidthType } = require("docx");

class Adapter {
  constructor(component, resume) {
    this.docx = component.docx
    this.resume = resume.metadata
  }

  generatePersonalInfo() {
    const {
      Paragraph,
      TextRun
    } = this.docx

    let personalInfo = []

    let keysPersonalInfo = Object.keys(this.resume)
    keysPersonalInfo.forEach((keyPersonalInfo)=>{
      const valuePersonalInfo = this.resume[keyPersonalInfo]
      if(valuePersonalInfo && 'object' !== typeof(valuePersonalInfo)) {
        const para = new Paragraph({
          spacing: {
            before: 20
          },
          children: [
            new TextRun({
              text: `${keyPersonalInfo}: `,
              bold: true
            }),
            new TextRun(valuePersonalInfo),
          ]
        })
        personalInfo.push(para)
      }
    })

    return personalInfo
  }

  generateEducationExperience() {
    const {
      Paragraph,
      TextRun,
      TableRow,
      TableCell,
      Table,
      WidthType
    } = this.docx

    let educationExperiences = []
    if(
      !this.resume
      || !this.resume.education_experience
      || !this.resume.education_experience.length
    ) {
      return educationExperiences
    }
    this.resume.education_experience.forEach((item) => {
      const tableRowEducationExperience = new TableRow({
        children: [
          new TableCell({
            width:{
              size: 30,
              type: WidthType.PERCENTAGE
            },
            children: [
              new Paragraph({
                children: [
                  new TextRun({
                    text: `${item.start_time} - ${item.end_time}`,
                    bold: true,
                  })
                ],
                indent: {
                  left: 200,
                  end: 200
                }
              })
            ],
          }),
          new TableCell({
            width:{
              size: 70,
              type: WidthType.PERCENTAGE
            },
            children: [
              new Paragraph({
                text: item.school_name,
                indent: {
                  left: 200,
                  end: 200
                }
              }),
              new Paragraph({
                text: item.major,
                indent: {
                  left: 200,
                  end: 200
                },
                spacing: {
                  before: 200
                }
              })
            ],
          })
        ]
      })
      educationExperiences.push(tableRowEducationExperience)
    });


    let tableEducationExperiences = new Table({
      rows: educationExperiences,
      margins: {
        top: 100
      },
      columnWidths: [
        400,
        1000
      ]
    })

    return [tableEducationExperiences]
  }

  generateWorkExperience() {
    const {
      Paragraph,
      TextRun,
      TableRow,
      TableCell,
      Table
    } = this.docx
    let workExperiences = []

    if(
      !this.resume
      || !this.resume.work_experience
      || !this.resume.work_experience.length
    ) {
      return workExperiences
    }

    this.resume.work_experience.forEach((item) => {
      const tableRowWorkExperience = new TableRow({
        children: [
          new TableCell({
            width:{
              size: 30,
              type: WidthType.PERCENTAGE
            },
            children: [
              new Paragraph({
                indent: {
                  left: 200,
                  end: 200
                },
                children: [
                  new TextRun({
                    text: `${item.start_time} - ${item.end_time}`,
                    bold: true,
                  })
                ]
              })
            ],
          }),
          new TableCell({
            width:{
              size: 70,
              type: WidthType.PERCENTAGE
            },
            children: [
              new Paragraph({
                text: item.company_name,
                indent: {
                  left: 200,
                  end: 200
                },
              }),
              new Paragraph({
                indent: {
                  left: 200,
                  end: 200
                },
                text: item.position,
                spacing: {
                  before: 200
                }
              }),
              new Paragraph({
                indent: {
                  left: 200,
                  end: 200
                },
                spacing: {
                  before: 200
                },
                children: [
                  new TextRun({
                    text: "LANGUAGE & TECHNOLOGY: ",
                    bold: true,
                  }),
                  new TextRun(item.skill_set)
                ]
              }),

            ],
          })
        ]
      })
      workExperiences.push(tableRowWorkExperience)
    });

    let tableWorkExperiences = new Table({
      rows: workExperiences,
      margins: {
        top: 100
      },
      columnWidths: [
        400,
        1000
      ]
    })

    return [tableWorkExperiences]
  }
  async generateWord() {
    const {
      Document,
      Packer,
      Paragraph,
      TextRun,
      HeadingLevel,
      AlignmentType
    } = this.docx

    const doc = new Document();

    // Documents contain sections, you can have multiple sections per document, go here to learn more about sections
    // This simple example will only contain one section
    doc.addSection({
      properties: {},
      children: [
        new Paragraph({
          text: this.resume.name,
          heading: HeadingLevel.TITLE,
          alignment: AlignmentType.CENTER,
        }),
        new Paragraph({
          text: "Personal Information",
          heading: HeadingLevel.HEADING_1,
          spacing: {
            before: 200,
            after:200
          }
        }),
        ...this.generatePersonalInfo(),
        new Paragraph({
          text: "Education",
          heading: HeadingLevel.HEADING_1,
          spacing: {
            before: 200,
            after:200
          }
        }),
        ...this.generateEducationExperience(),
        new Paragraph({
          text: "Work Experience",
          heading: HeadingLevel.HEADING_1,
          spacing: {
            before: 200,
            after:200
          }
        }),
        ...this.generateWorkExperience()
      ]
    });

    // Used to export the file into a .docx file
    const b64string = await Packer.toBase64String(doc)

    return b64string
  }
}

module.exports = {
  Adapter
}