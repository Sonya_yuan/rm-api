'use strict';
const jwt = require('jsonwebtoken');
const md5 = require('md5-node');
var moment = require('moment');

async function getJsonWebToken (request, h) {
  const { payload } = request;
  const clientAppId = payload.clientId;
  const clientAppSecret = payload.clientSecret;

  if (clientAppId === process.env.CLIENT_APPID) {      
    console.log('Client App Id matched!');
    const userName = payload.userName;
    const userPassword = md5(payload.userPass, process.env.PASSWORD_MD5_KEY);
    // const userPassword = userName === 'ShinetechXX01' ? 'e10adc3949ba59abbe56e057f20f883e' : md5(payload.userPass, process.env.PASSWORD_MD5_KEY);
    const { User } = request.server.app.db.model;
    const data = await User.query()
    .select('id', 'name', 'user_name', 'role')
    .where('name', userName)
    .where('password', userPassword)
    .catch((ex) => {
        console.log(ex);
        data = [{ id: -1 }];
    });
    let userEntity = null;
    data.map((item, idx) => {
      if (item.id > 0) {
        userEntity = {
          accountId: item.id, 
          name: item.name, 
          role: item.role.split(','),
          //scope: ['system-admin', 'user']
          scope: item.role.split(',')
        };
      }
    });
    if (userEntity) {
      userEntity.exp=parseInt(moment.utc().add(1,'days').format('X'));
      console.log(userEntity);
      var token = jwt.sign(userEntity, process.env.JWT_PRIVATE_KEY, { algorithm: 'HS256'} );
      console.log(token);
      return h.response(token);
    } else {
      console.log('user not matched!');
      return h.response(null);
    }    
  } else {
    console.log('Client App Id not matched!');
    return h.response(null);
  }
}

module.exports = { getToken: getJsonWebToken };