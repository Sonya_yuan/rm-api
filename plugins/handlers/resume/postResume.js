"use strict";

async function addResume(request, h) {
  const { payload } = request;
  const { Resume, ResumeAudit } = request.server.app.db.model;
  const { id, resume } = payload;

  let response = { result: true };
  if (id) {
    // Update resume
    const numUpdated = await Resume.query()
      .findById(id)
      .patch({ metadata: resume });
    if (numUpdated === 1) {
      const insertedResumeAudit = await ResumeAudit.query().insert({
        audit_id: 1,
        resume_id: id,
        metadata: resume,
      });
    } else {
      response.result = false;
      response.message = "update resume failed";
    }
  } else {
    // Insert resume
    const insertedResume = await Resume.query().insert({
      created_by: 1,
      metadata: resume,
    });
    response.data = insertedResume;
  }

  return h.response(response);
}

module.exports = addResume;
