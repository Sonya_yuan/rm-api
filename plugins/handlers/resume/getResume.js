"use strict";

async function getResume(request, h) {
  const { Resume } = request.server.app.db.model;
  const { params } = request;
  let response = {};
  const dbResume = await Resume.query().findById(params.id);

  if (dbResume) {
    response = {
      result: true,
      data: dbResume,
    };
  } else {
    response = {
      result: false,
      error:'no resume found'
    };
  }

  return h.response(response);
}

module.exports = getResume;
