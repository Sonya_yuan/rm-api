'use strict';

async function getResumeAssessment (request, h) {
  const {
    Boom,
    pdfMake,
    fs,
    generatePdf,
    bufferConcat
  } = request.server.app;
  const { Resume } = request.server.app.db.model;

  const { params } = request;

  const resumeId = params.id? params.id: 0;

  if(!resumeId) {
    throw Boom.notFound("Resume id missing");
  }

  const resumes = await Resume.query()
  .where("id", resumeId)
  .catch((ex) => {
    throw Boom.badRequest("Get resume failed");
  })

  if(!resumes || !resumes.length) {
    throw Boom.notFound("Not found resume");
  }

  const resumeModel = resumes[0];
  const components = {
    pdfMake,
    generatePdf,
    fs,
    bufferConcat
  }

  const GeneratePdfAdapter = generatePdf.Adapter;
  const generatePdfAdapter = new GeneratePdfAdapter(components, resumeModel);

  const data = await generatePdfAdapter
  .generatePdf()

  return h.response(data);
}

module.exports = getResumeAssessment;
