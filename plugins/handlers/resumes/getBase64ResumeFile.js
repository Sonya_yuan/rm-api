"use strict";

async function getBase64ResumeFile(request, h) {
  //create file and convert to base64
  let response = { result: false };

  const { Boom, simpleTemplateFile } = request.server.app;
  
  const { params } = request;

  const resumeId = params.resumeId ? params.resumeId : "";
  const fileFormat = params.format ? params.format : "doc";

  if (!resumeId) {
    throw Boom.notFound("resumeId is missing");
  }

  const { Resume } = request.server.app.db.model;
  const resumes = await Resume.query()
    .where("id", resumeId)
    .catch((ex) => {
      throw Boom.badRequest("Get resume failed");
    });

  if (!resumes || !resumes.length) {
    throw Boom.notFound("Not found resume");
  }
  let resumeModel = resumes[0];
  //todo:

  try {
    let fileBuff;
    switch (fileFormat) {
      case "doc":
        fileBuff = simpleTemplateFile(resumeModel, parseInt(resumeModel.id));
        break;

      default:
        break;
    }

    if (!fileBuff) {
      throw Boom.notFound("Not found fileName");
    }

    fileBuff = fileBuff.toString("base64");
    response.result = true;
    response.data = fileBuff;
  } catch (error) {
    console.log(error);
    response.message = error;
  }

  return h.response(response);
}

module.exports = getBase64ResumeFile;
