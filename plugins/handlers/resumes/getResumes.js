'use strict';

async function getResumes (request, h) {
  const { Resume } = request.server.app.db.model;
  const { ref, withGraphFetched  } = request.server.app.objection;

  const { query } = request;

  const limit = query.limit? query.limit: 10;
  const page = query.page? query.page: 0;
  const data = await Resume.query()
  .select(
    'id',
    ref('metadata:title')
      .castText()
      .as('title'),
    ref('metadata:name')
    .castText()
    .as('name'),
    'created_at',
  )

  .withGraphFetched('owner(selectName)')
  .modifiers({
    selectName(builder) {
      builder.select("name");
    }
  })
  .orderBy("id", "desc")
  .limit(limit)
  .offset(page)
  .catch((ex) => {
    console.log(ex);
  })


  return h.response(data);

}

module.exports = getResumes;
