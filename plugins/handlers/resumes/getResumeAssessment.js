'use strict';

async function getResumeAssessment (request, h) {
  const {
    Boom,
    docx,
    fs,
    generateWord
  } = request.server.app;
  const { Resume } = request.server.app.db.model;

  const { params } = request;

  const resumeId = params.id? params.id: 0;

  if(!resumeId) {
    throw Boom.notFound("Resume id missing");
  }

  const resumes = await Resume.query()
  .where("id", resumeId)
  .catch((ex) => {
    throw Boom.badRequest("Get resume failed");
  })

  if(!resumes || !resumes.length) {
    throw Boom.notFound("Not found resume");
  }

  const resumeModel = resumes[0];
  const components = {
    docx,
    generateWord
  }

  const GenerateWordAdapter = generateWord.Adapter;
  const generateWordAdapter = new GenerateWordAdapter(components, resumeModel);

  const data = await generateWordAdapter
  .generateWord()

  return h.response(data);
}

module.exports = getResumeAssessment;
