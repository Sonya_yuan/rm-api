'use strict';

async function generateUserResume (request, h) {
  const {
    Boom,
    docx,
    fs,
    generateWord,
    simpleTemplateFile
  } = request.server.app;

  let response = { result: true };

  const { Resume } = request.server.app.db.model;
  const { payload } = request;
  const { params } = request;
  const resumeId = payload.resumeId;
  const format = payload.format;
  
  if (!resumeId) {
    throw Boom.notFound("Resume id missing");
  }
   // 
  const resumes = await Resume.query()
  .where("id", resumeId)
  .catch((ex) => {
    throw Boom.badRequest("Get resume failed");
  })
  // 
  if (!resumes || !resumes.length) {
    throw Boom.notFound("Not found resume");
  }
  const resumeModel = resumes[0];
  // 
  const fileName = simpleTemplateFile(resumeModel, parseInt(resumeModel.id));

  resumeModel.metadata["docFile"] = fileName;

  const numUpdated = await Resume.query()
    .findById(resumeId)
    .patch({ metadata: resumeModel.metadata });
  if (numUpdated === 1) {
    response.fileName = fileName;
  } else {
    response.result = false;
    response.message = "update resume failed";
  }

  return h.response(response);
}

module.exports = generateUserResume;
