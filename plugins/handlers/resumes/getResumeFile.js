"use strict";

async function getResumeFile(request, h) {
  let response = { result: false };

  const { Boom, fs, path } = request.server.app;

  const { params } = request;

  const resumeId = params.resumeId ? params.resumeId : "";
  const fileFormat = params.format ? params.format : "doc";

  if (!resumeId) {
    throw Boom.notFound("resumeId is missing");
  }

  const { Resume } = request.server.app.db.model;
  const resumes = await Resume.query()
    .where("id", resumeId)
    .catch((ex) => {
      throw Boom.badRequest("Get resume failed");
    });

  if (!resumes || !resumes.length) {
    throw Boom.notFound("Not found resume");
  }
  let resumeModel = resumes[0];
  let fileName = "";
  switch (fileFormat) {
    case "doc":
      fileName = resumeModel.metadata.docFile;
      break;

    default:
      break;
  }

  if (!fileName) {
    throw Boom.notFound("Not found fileName");
  }

  const resumeFilePath = path.resolve(
    __dirname,
    "../../../resumes/" + resumeModel.id + "/" + fileName
  );

  let data;
  try {
    data = fs.readFileSync(resumeFilePath);
    data = Buffer.from(data).toString("base64");
    response.result = true;
    response.data = data;
  } catch (error) {
    console.log(error);
    response.message = error;
  }

  return h.response(response);
}

module.exports = getResumeFile;
