"use strict";

async function generateUserOwnResume(request, h) {
  const {
    Boom,
    docx,
    fs,
    generateWord,
    simpleTemplateFile,
  } = request.server.app;

  let response = { result: true };

  let tokenUserId = -1;
  if (request.auth.isAuthenticated) {
    tokenUserId = request.auth.credentials.accountId;
  }
  const { Resume } = request.server.app.db.model;
  const { payload } = request;
  const format = payload.format;
  const resumeId = tokenUserId;

  if (!resumeId) {
    throw Boom.notFound("Resume id missing");
  }
  if (tokenUserId < 0) {
    throw Boom.notFound("Token user id missing");
  }
  //
  const resumes = await Resume.query()
    .where("id", resumeId)
    .catch((ex) => {
      throw Boom.badRequest("Get resume failed");
    });
  //
  if (!resumes || !resumes.length) {
    throw Boom.notFound("Not found resume");
  }
  let resumeModel = resumes[0];
  //
  const fileName = simpleTemplateFile(resumeModel, tokenUserId);

  resumeModel.metadata["docFile"] = fileName;

  const numUpdated = await Resume.query()
    .findById(resumeId)
    .patch({ metadata: resumeModel.metadata });
  if (numUpdated === 1) {
    response.fileName = fileName;
  } else {
    response.result = false;
    response.message = "update resume failed";
  }

  return h.response(response);
}

module.exports = generateUserOwnResume;
