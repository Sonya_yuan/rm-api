"use strict";

const USERS_TABLE_NAME = "users";
const RESUMES_TABLE_NAME = "resumes";
const RESUME_AUDIT_TABLE_NAME = "resumeAudit";

module.exports = {
  USERS_TABLE_NAME,
  RESUMES_TABLE_NAME,
  RESUME_AUDIT_TABLE_NAME,
};