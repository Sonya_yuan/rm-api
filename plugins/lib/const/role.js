"use strict";

const SYSTEM_ADMIN = "system-admin";
const INTERVIEWEE = "interviewee";
const RECRUITER="recruiter";

module.exports =  {
  SYSTEM_ADMIN,
  INTERVIEWEE,
  RECRUITER,
};