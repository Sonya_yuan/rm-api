"use strict";

const Knex = require("knex");
const objection = require("objection");
const path = require("path");
const knexFile = require("./knexfile.js");
const User = require("./models/user.js");
const Resume = require("./models/resume");
const ResumeAudit = require("./models/resumeAudit");

const { Model } = objection;
const knex = Knex(knexFile["local"]);
Model.knex(knex);

const initModelHash = {
  User,
  Resume,
  ResumeAudit,
};
const modelHash = {};

Object.keys(initModelHash).forEach(function (modelName) {
  const model = initModelHash[modelName](Model, modelHash);
  modelHash[modelName] = model;
});

async function migrate() {
  await knex.migrate.latest({
    directory: path.resolve(__dirname, "./migrations"),
  })
  .catch((err) => {
    console.log(err);
  });
}


module.exports = {
  knex,
  objection,
  migrate,
  model: modelHash,
};