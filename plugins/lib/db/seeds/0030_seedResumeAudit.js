"use strict";

const dbTableName = require("../../const/dbTableName");

const tableName = dbTableName.RESUME_AUDIT_TABLE_NAME;

exports.seed = async (knex, Promise) => {
  await knex(tableName).truncate();
  console.log(`--- ${tableName} table truncated.`);

  const apps = [
    {
      id: 1,
      audit_id: 1,
      resume_id: 1
    },
  ];

  const batchSize = 100.0;
  const batchCount = Math.ceil(apps.length / batchSize);
  for (let i = 0; i < batchCount; i++) {
    const appBatch = apps.slice(i * batchSize, (i + 1) * batchSize);
    console.log(`Inserting users ${i * batchSize} to ${((i + 1) * batchSize) - 1}`);
    await knex(tableName).insert(appBatch);
  }

  console.log(`+++ ${tableName} table seeded.`);
};
