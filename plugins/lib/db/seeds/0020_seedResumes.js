"use strict";

const dbTableName = require("../../const/dbTableName");

const tableName = dbTableName.RESUMES_TABLE_NAME;

exports.seed = async (knex, Promise) => {
  await knex(tableName).truncate();
  console.log(`--- ${tableName} table truncated.`);

  const apps = [
    {
      id: 1,
      created_by: 1,
      metadata: {
        name: "Aaron Yu",
        email: "yujj@shinetechsoftware.com",
        phone: "13331776071",
        title: "Test",
        gender: "male",
        birthday: "1982-04-29T16:00:00.000Z",
        languages: [
          {
            id: "0C547A96-E9C0-8C14-18D1-B760EF1651C0",
            name: "English",
            description: "CET 4. Be good at reading and writing.",
          },
          {
            id: "10BBE3D0-FB0C-7B48-08F9-025311A256E3",
            name: "Japan",
            description: "No good at this",
          },
        ],
        responsive_roles: ["Senior Software Engineer"],
        self_evaluations: [
          {
            id: "CDBFD691-941D-6524-3227-BAD3B8D5C19D",
            line:
              "More than 13 years of experiences with software development, including more than 12 years of web development experiences and 1 year’s experience with WPF technology",
            isEdit: false,
          },
          {
            id: "6187CFE3-59FF-FA15-35C8-F84BB1E72C7A",
            line:
              "Good at web, desktop, mobile technologies, like C#,.Net, Xamarin, HTML5,Node.js, Angular, etc.",
            isEdit: false,
          },
          {
            id: "E16461A0-2BEA-FE57-4E12-73C4DE371A8B",
            line:
              "Good at Agile development, more than 5 years of experience with JIRA, Vision One, 5pm, Trello, etc.",
            isEdit: false,
          },
          {
            id: "605B4CAC-9D46-79DD-C63C-86722663F672",
            line:
              "Solid software programming and architecture knowledge with strong analytical skill",
            isEdit: false,
          },
          {
            id: "75360153-CC7D-5521-C456-9D3CB6515922",
            line: "Independent problem-solving skills and teamwork spirit",
          },
        ],
        work_experiences: [
          {
            id: "7B8E6C39-D84A-F71A-BE02-3C7FEE555B62",
            other: "Hapi, Express, Bootstrap, JQuery, Handlerbars, OAuth",
            database: "PostgreSql",
            end_time: "2020-09-11T16:00:00.000Z",
            position: "developer",
            skill_set: "ASP.NET Angular Node Vue React",
            start_time: "2020-08-31T16:00:00.000Z",
            methodology: "Agile",
            company_name: "shinetech",
            project_name: "RM",
            work_description:
              "This is a very challenging job. We must keep our skillset up to date.",
          },
        ],
        technical_summary: [
          {
            id: "D05CC9A6-4969-A1B2-CC4F-72C35B761852",
            name: "languages",
            tags: ["C#", "JS", "Html5 & CSS3"],
            isEdit: false,
            description: "",
          },
          {
            id: "C95394BD-E75B-3A70-B9DC-1406726D9908",
            name: "DBs",
            tags: ["SqlServer", "Oracle"],
            isEdit: false,
            description: "",
          },
          {
            id: "A5407E95-E8AF-CB57-0889-1CCC17547860",
            name: "WebStacks",
            tags: ["ASP .NET Web Form", "ASP .NET MVC"],
            isEdit: false,
            description: "",
          },
          {
            id: "81ADA3EB-5C56-82B6-5352-649139F118C7",
            name: "OtherFrameworks",
            tags: ["Entity Framework", "Knex"],
            isEdit: false,
            description: "",
          },
          {
            id: "824FFE7F-E5B0-887F-88A8-0879C551DC83",
            name: "OrdinaryIDEs",
            tags: ["Visual Studio", "Visual Studio Code"],
            isEdit: false,
            description: "",
          },
          {
            id: "1FAFCC12-6A33-D57C-FC28-5D3AD4D68112",
            name: "OtherTools",
            tags: ["SVN", "GIT"],
            isEdit: false,
            description: "",
          },
          {
            id: "E4F8D50F-079A-EE00-5E9B-DDF21945A6A4",
            name: "Methodologies",
            tags: ["OOP"],
            description: "",
          },
          {
            id: "7958DA75-52A1-D54C-C064-6A15B5DD530C",
            name: "UnitTests",
            tags: ["Jest", "Mocha"],
            isEdit: false,
            description: "",
          },
        ],
        project_experiences: [
          {
            id: "AAC506C7-98BD-AC26-FA62-CED9A95E17C4",
            name: "Austcover",
            lines: [
              {
                id: "001",
                name: "Main Tasks",
                tags: [
                  "Daily maintenance of internal system, fix issues",
                  "Design of new management report in the internal system",
                  "Upgrade the portal website PHP / WordPress / plugin",
                  "Daily maintenance of the portal website refers to the modification of themes specified by customers",
                ],
              },
              {
                id: "002",
                name: "Technology & Language",
                tags: ["C#/ASP.NET WEB Forms", "Php", "MySQL"],
              },
              {
                id: "003",
                name: "Development Environment & Tools",
                tags: ["Visual Studio Code", "Visual Studio"],
              },
            ],
            isEdit: false,
            end_time: "2021-05-20",
            start_time: "2021-03-04",
            description:
              "This is an insurance service company, providing various insurance services for customers. It has a portal website, which is customized by WordPress. There is one inside ASP.NET Webform system to manage internal employees and insurance policies.",
          },
        ],
        education_experiences: [
          {
            id: "409F68A2-8FB1-B00C-77DF-2CEA6BEF3452",
            major: "Computer Science and Technology",
            isEdit: false,
            end_time: "2004-03-31T16:00:00.000Z",
            start_time: "2001-09-01T16:00:00.000Z",
            school_name: "Jilin Provincial Institute of Education",
          },
          {
            id: "AA4146A8-2909-9B70-C57E-B019DABF6A8E",
            major: "Humanility",
            isEdit: false,
            end_time: "2021-05-19T16:00:00.000Z",
            start_time: "2021-03-03T16:00:00.000Z",
            school_name: "New School",
          },
        ],
      }
    },
  ];

  const batchSize = 100.0;
  const batchCount = Math.ceil(apps.length / batchSize);
  for (let i = 0; i < batchCount; i++) {
    const appBatch = apps.slice(i * batchSize, (i + 1) * batchSize);
    console.log(
      `Inserting resumes ${i * batchSize} to ${(i + 1) * batchSize - 1}`
    );
    await knex(tableName).insert(appBatch);
  }

  console.log(`+++ ${tableName} table seeded.`);
};
