"use strict";

 const dbTableName = require("../../const/dbTableName");
 const resume = require("../models/resume");

 module.exports = function (Model, modelHash) {
  class user extends Model {
    static get tableName() {
      return dbTableName.USERS_TABLE_NAME;
    }

    static get relationMappings() {
      return {
        resumes: {
          relation: Model.HasManyRelation,
          modelClass: resume,
          join: {
            from: "user.id",
            to: "resume.created_by",
          }
        }
      }
    }
  }

  return user;
};
