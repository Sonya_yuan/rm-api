"use strict";

 const dbTableName = require("../../const/dbTableName");

 module.exports = function (Model, modelHash) {
  class resumeAudit extends Model {
    static get tableName() {
      return dbTableName.RESUME_AUDIT_TABLE_NAME;
    }

  }

  return resumeAudit;
};
