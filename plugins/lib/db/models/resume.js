"use strict";

 const dbTableName = require("../../const/dbTableName");

 module.exports = function (Model, modelHash) {
  class resume extends Model {
    static get tableName() {
      return dbTableName.RESUMES_TABLE_NAME;
    }

    static get relationMappings() {
      const {User, ResumeAudit} = modelHash;
      return {
        resumeAudits: {
          relation: Model.HasManyRelation,
          modelClass: ResumeAudit,
          join: {
            from: "resumes.id",
            to: "resumeAudit.resume_id",
          }
        },
        owner: {
          relation: Model.BelongsToOneRelation,
          modelClass: User,
          join: {
            from: "resumes.created_by",
            to: "users.id",
          }
        }
      }
    }
  }

  return resume;
};
